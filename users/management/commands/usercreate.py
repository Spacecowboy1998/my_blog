from django.contrib.auth.models import User
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'DB random user generator'

    def add_arguments(self, parser):
        parser.add_argument('info', type=str, help='Information about User')
        parser.add_argument('-a', '--admin', action='store_true', help='Define an admin account')

    def handle(self, *args, **kwargs):
        info = kwargs['info'].split(' ')
        qty = int(len(info)/2)
        admin = kwargs['admin']

        for i in range(qty):
            username = info[i*2]
            password = info[i*2+1]
            if User.objects.filter(username=username):
                self.stdout.write(f'User with {username} already exists!')
            else:
                if admin:
                    User.objects.create_superuser(username=username, email='', password=password)
                else:
                    User.objects.create_user(username=username, email='', password=password)

            self.stdout.write(f'User created successfully! username: {username}, password: {password}')