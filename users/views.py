from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMessage
from django.db.models import Count
from django.shortcuts import render, redirect
from django.contrib import messages, auth
from django.contrib.auth.decorators import login_required
from django.utils.encoding import force_bytes, force_str
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.views import View
from django.urls import reverse
from .token import account_activation_token
from .forms import UserRegisterForm, UserUpdateForm, ProfileUpdateForm, LoginForm
from django.contrib.auth.models import User


def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'{username} your account created successfully !')
            user.is_active = False
            user.save()

            uidb64 = urlsafe_base64_encode(force_bytes(user.pk))
            token = account_activation_token.make_token(user)
            domain = get_current_site(request).domain
            link = reverse('activate', kwargs={
                'uidb64': uidb64, 'token': token
            })

            activate_url = 'http://' + domain + link
            email_subject = 'Account Activation'
            email_body = f'Hello {user.username}! \nPlease follow this link to activate you account!\n{activate_url}'
            email = EmailMessage(
                email_subject,
                email_body,
                'noreply@gmail.com',
                [user.email]
            )
            email.send(fail_silently=False)
            return redirect('login')
    else:
        form = UserRegisterForm()
    return render(request, 'users/register.html', {'form': form})


class VerificationView(View):
    def get(self, request, uidb64, token):
        try:
            id = force_str(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=id)

            if not account_activation_token.check_token(user, token):
                messages.error(request, 'User already activated!')
                return redirect('login')

            if user.is_active:
                return redirect('login')
            user.is_active = True
            user.save()

            messages.success(request, 'Account activated successfully!')
            return redirect('login')

        except Exception as ex:
            pass

        return redirect('login')


class LoginView(View):
    def get(self, request):
        context = {
            'form': LoginForm()
        }
        return render(request, 'users/login.html', context)

    def post(self, request):
        context = {
            'form': LoginForm()
        }
        username = request.POST['username']
        password = request.POST['password']
        if username and password:
            user = auth.authenticate(username=username, password=password)

            if user:
                if user.is_active:
                    auth.login(request, user)
                    messages.success(request, 'Welcome, ' +
                                     user.username+' you are now logged in')
                    return redirect('blog_index')
                messages.error(
                    request, 'Account is not active,please check your email')
                return render(request, 'users/login.html')
            messages.error(
                request, 'Invalid credentials, please try again')
            return render(request, 'users/login.html', context)

        messages.error(
            request, 'Please fill all fields')
        return render(request, 'users/login.html')



@login_required
def profile(request):
    if request.method == 'POST':
        u_form = UserUpdateForm(request.POST,
                                instance=request.user)
        p_form = ProfileUpdateForm(request.POST,
                                   request.FILES,
                                   instance=request.user.profile)

        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            username = u_form.cleaned_data.get('username')
            messages.success(request, f'{username} your account updated successfully !')
            return redirect('profile')
    else:
        u_form = UserUpdateForm(instance=request.user)
        p_form = ProfileUpdateForm(instance=request.user.profile)

    context = {
        'u_form': u_form,
        'p_form': p_form,
        'post': User.objects.annotate(num_post=Count('post')).get(username=request.user.username)
    }
    return render(request, 'users/profile.html', context)

