from django.urls import path
from . import views


urlpatterns = [
    path('register/', views.register, name='register'),
    path('profile/', views.profile, name='profile'),
    path('activate/<uidb64>/<token>', views.VerificationView.as_view(), name='activate'),
    path('login/', views.LoginView.as_view(), name='login')
]
