from django.db import models
from django.contrib.auth.models import User
from PIL import Image


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(default='default.png', upload_to='profile_pics')
    bio = models.CharField(max_length=20)

    def __str__(self):
        return f'{self.user.username} profile'

    def save(
        self, force_insert=False, force_update=False, using=None, update_fields=None
    ):
        super().save()

        img = Image.open(self.image.path)

        if img.height > 300 or img.width > 300:
            img_resize = (300, 300)
            img.thumbnail(img_resize)
            img.save(self.image.path)
